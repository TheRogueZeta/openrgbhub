#include "test_effect.hpp"
#include "utils.hpp"

TestEffect::TestEffect(effect_param_t * effect_param, Zone * zone_pointer) : Effect(effect_param, zone_pointer)
{
    last_cycle = getTick();
    period     = 20000 / effect_param->speed;
    reversed = param.direction_bitfield == MODE_PARAM_DIRECTION_RIGHT;
    if (param.color_mode != EFFECT_PARAM_COLOR_MODE_SPECIFIC){
        param.color_array[0].r = 255;
        param.color_array[0].g = 255;
        param.color_array[0].b = 255;
    }
}

void TestEffect::update()
{
    uint8_t size = zone_ptr->getInfo()->size;
    uint32_t elapsed_time = getTick() - last_cycle;
    
    if (elapsed_time >= period) 
    {
        last_cycle = getTick();
        return;
    }

    uint8_t index = (elapsed_time * size) / period;
    if (reversed) index = (size - 1) - index;
    zone_ptr->clear();
    zone_ptr->setLED(
        index, 
        param.color_array[0].r, 
        param.color_array[0].g, 
        param.color_array[0].b 
    );
    
    return;
}

void TestEffect::zoneHasBeenResized()
{
    return;
}

Effect * testEffectFactory(effect_param_t * effect_parameters, Zone * zone_ptr)
{
    return new TestEffect(effect_parameters, zone_ptr);
}


effect_constructor_t TestEffectConstructor =
{
    {
        EFFECT_FLAGS_HAS_SPEED 
        | EFFECT_FLAGS_HAS_DIRECTION_LR, /* Feature bitfield */
        EFFECT_FLAGS_HAS_MODE_SPECIFIC_COLORS,  /* Mode type */
        1,                              /* Min speed */
        20,                             /* Max speed */
        0,                              /* Min brightness */
        0,                              /* Max brightness */
        1,                              /* Min size of color array */
        1,                              /* Max size of color array */
        "Test effect"                   /* Name */
    },
    &testEffectFactory
};