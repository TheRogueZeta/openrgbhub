#ifdef USE_ARDUINO_TEMPERATURE_SENSOR

#include "arduino_temperature_sensor.hpp"

#include <Arduino.h>

#define ADC_MEASUREMENTS 10

ArduinoTempSensor::ArduinoTempSensor(uint8_t analog_pin, uint32_t fixed_r, uint32_t nominal_r, uint16_t beta, const char * name)
{
    this->analog_pin = analog_pin;
    this->fixed_r = fixed_r;
    this->nominal_r = nominal_r;
    this->beta = beta;

    sensor_info.name = name;
    sensor_info.type = SENSOR_TYPE_TEMP;
}

void ArduinoTempSensor::begin()
{
    pinMode(analog_pin, INPUT);
}

uint16_t ArduinoTempSensor::read()
{
    float adc_average = 0;
    
    for (int i = 0; i < ADC_MEASUREMENTS; i++)
    {
        adc_average += analogRead(analog_pin);
    }

    adc_average /= float(ADC_MEASUREMENTS);

    float Rt = (float)fixed_r / (1023.0 / adc_average - 1.0);
    float st = 1.0 / 298.15;

    st += log((float)Rt / (float)nominal_r) / (float)beta;
    st = 1.0 / st;

    if (st > 655.35) return 65535;

    return uint16_t(st * 100.0);
}

#endif