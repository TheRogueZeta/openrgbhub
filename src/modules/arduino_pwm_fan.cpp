#ifdef USE_ARDUINO_PWM_FAN

#include "arduino_pwm_fan.hpp"

ArduinoPWMFan::ArduinoPWMFan(pwm_fan_out_pin_t out, uint8_t in, const char * name)
{
    this->out = out;
    this->in = in;

    info.features = 0 | FAN_FLAGS_HAS_RPM_FEEDBACK;
    info.name = name;

    autoregister_isr(in, &time_buf);

    return;
}

void ArduinoPWMFan::begin()
{
    switch (out) {
        case LEONARDO_PIN_6:
            setupTimersLeonardo();
            setupPin6Leonardo();
            break;
        case LEONARDO_PIN_10:
            setupTimersLeonardo();
            setupPin10Leonardo();
            break;
        default:
            break;

    }
}

void ArduinoPWMFan::setSpeedAbsolute(uint16_t speed)
{
    switch (out) {
        case LEONARDO_PIN_6:
            setPin6Leonardo(speed / 256); 
            break;
        case LEONARDO_PIN_10:
            setPin10Leonardo(speed / 256);
            break;
        default:
            break;
    }
    
    return;
}

void ArduinoPWMFan::setSpeedRPM(uint16_t speed)
{
    return;
}

uint16_t ArduinoPWMFan::getRPMFeedback()
{
  uint32_t start_time = time_buf.getHead();
  uint32_t stop_time = time_buf.getTail();
  if ((micros() - stop_time) > 250000) return 0;

  for (uint8_t k = 0; k < (time_buf.size() - 2); k++)
  {
    uint32_t start = time_buf.get(k);
    uint32_t stop  = time_buf.get(k + 1);
    if ((stop - start) > 250000) return cached_rpm;
  }

  cached_rpm = uint32_t(uint32_t(time_buf.size() - 1) * uint32_t(30) * uint32_t(1000000)) / (stop_time - start_time);
  return cached_rpm;
}

void ArduinoPWMFan::setupTimersLeonardo()
{
  /* Set FastPWM for all pins */
  TCCR4D &= (0b11111100);

  /* Set timer prescaler to x8, meaning 48MHz / 8 = 6Mhz */
  TCCR4B = 4;
  
  /* PLL Configuration */
  /* Use 96MHz / 2 = 48MHz */
  /* Otherwise a prescaler of 1.5 will be used, resulting in 96MHz / 1.5 = 64MHz,
    thus making it impossible to get the proper frequency without losing resolution */
  PLLFRQ = (PLLFRQ & 0xCF) | 0x30;
}

void ArduinoPWMFan::setupPin6Leonardo()
{ 
  /* Set pin 7 on PORTD (D6 on the board) as output */
  DDRD |= 1 << 7;   
  /* Clear on compare match (and only connect OC4D to PWM) */
  /* Force updating compare match (FOC4D) */
  /* Enable PWM on OCR4D (PWM4D) */
  TCCR4C |= (1 << 3) | (1 << 1) | (1 << 0);  
}

void ArduinoPWMFan::setupPin10Leonardo()
{
  /* Set pin 6 on PORTB (D10 on the board) as output */
  DDRB |= 1 << 6;
  /* Clear on compare match (and only connect OC4B to PWM) */
  /* Force updating compare match (FOC4B) */
  /* Enable PWM on OCR4B (PWM4B) */
  TCCR4A |= (1 << 5) | (1 << 2) | (1 << 0);
}

void ArduinoPWMFan::setPin6Leonardo(uint8_t value)
{
  /* Set output compare register to desired value */
  OCR4D = value;   
}

void ArduinoPWMFan::setPin10Leonardo(uint8_t value)
{
  /* Set output compare register to desired value */
  OCR4B = value;
}

/* Autoregisters an isr to one of the handlers which holds a handle to the time buffer */
/* This is ugly, but you can't attach method to the interrput*/
void autoregister_isr(uint8_t pin, RotBuf<uint32_t, TIME_ARRAY_SIZE> * buf)
{  
    if (isr_handler_ptr_1 == nullptr){  
        isr_handler_ptr_1 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_1, FALLING);
        return;
    }

    if (isr_handler_ptr_2 == nullptr){  
        isr_handler_ptr_2 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_2, FALLING);
        return;
    }

    if (isr_handler_ptr_3 == nullptr){  
        isr_handler_ptr_3 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_3, FALLING);
        return;
    }
    if (isr_handler_ptr_4 == nullptr){  
        isr_handler_ptr_4 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_4, FALLING);
        return;
    }
    if (isr_handler_ptr_5 == nullptr){  
        isr_handler_ptr_5 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_5, FALLING);
        return;
    }
    if (isr_handler_ptr_6 == nullptr){  
        isr_handler_ptr_6 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_6, FALLING);
        return;
    }
    if (isr_handler_ptr_7 == nullptr){  
        isr_handler_ptr_7 = buf;
        attachInterrupt(digitalPinToInterrupt(pin), rpm_isr_handler_7, FALLING);
        return;
    }
    return;
}

RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_1 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_2 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_3 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_4 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_5 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_6 = nullptr;
RotBuf<uint32_t, TIME_ARRAY_SIZE> * isr_handler_ptr_7 = nullptr;

inline void rpm_isr_handler_com(RotBuf<uint32_t, TIME_ARRAY_SIZE> * buf){
    buf->push(micros());
    return;
}

void rpm_isr_handler_1(){ rpm_isr_handler_com(isr_handler_ptr_1); }
void rpm_isr_handler_2(){ rpm_isr_handler_com(isr_handler_ptr_2); }
void rpm_isr_handler_3(){ rpm_isr_handler_com(isr_handler_ptr_3); }
void rpm_isr_handler_4(){ rpm_isr_handler_com(isr_handler_ptr_4); }
void rpm_isr_handler_5(){ rpm_isr_handler_com(isr_handler_ptr_5); }
void rpm_isr_handler_6(){ rpm_isr_handler_com(isr_handler_ptr_6); }
void rpm_isr_handler_7(){ rpm_isr_handler_com(isr_handler_ptr_7); }

#endif