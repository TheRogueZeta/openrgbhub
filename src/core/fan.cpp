#include "fan.hpp"

void Fan::update(uint16_t sensor_val)
{
    if (state.function != FAN_FUNCTION_CURVE) return;

    uint16_t fan_speed = curveFindVal(sensor_val);

    switch (state.fan_mode)
    {
        case FAN_MODE_ABSOLUTE:
            setSpeedAbsolute(fan_speed);
            break;
        
        case FAN_MODE_RPM:
            setSpeedRPM(fan_speed);
            break;
            
        default:
            break;
    }
}

fan_info_t * Fan::getInfo()
{
    return &info;
}

uint16_t Fan::curveFindVal(uint16_t sensor_val)
{
    int16_t x1;
    int16_t y1;
    int16_t x2;
    int16_t y2;

    for (uint8_t i = 0; i < state.curve_array_size; i++)
    {
        if (state.curve_array[i].sensor_val >= sensor_val)
        {
            x2 = (int16_t)state.curve_array[i].sensor_val;
            y2 = (int16_t)state.curve_array[i].fan_val;

            if (i == 0) 
            {
                x1 = x2;
                y1 = y2;
            }
            else
            {
                x1 = (int16_t)state.curve_array[i - 1].sensor_val;
                y1 = (int16_t)state.curve_array[i - 1].fan_val;
            }

            break;
        }
        /* If the sensor reading is higher than the highest point */
        if (i == (state.curve_array_size - 1)) 
        {
            x2 = (int16_t)state.curve_array[i].sensor_val;
            y2 = (int16_t)state.curve_array[i].fan_val;

            x1 = x2;
            y1 = y2;
        }
    }

    if (x2 == x1) return (uint16_t)y1; 

    return ((y2 - y1) * (sensor_val - x2)) / (x2 - x1) + y2;
}

void Fan::setState(fan_state_t state)
{
    this->state = state;
    if (state.function == FAN_FUNCTION_FIXED)
    {
        if (state.fan_mode == FAN_MODE_ABSOLUTE)
        {
            setSpeedAbsolute(state.fixed_speed);
        }
        else
        {
            setSpeedRPM(state.fixed_speed);
        }
    }
}

fan_state_t * Fan::getState()
{
    return &state;
}