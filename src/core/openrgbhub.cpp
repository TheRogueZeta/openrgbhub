#include "openrgbhub.hpp"
#include "utils.hpp"
#include "openrgbprotocol.hpp"

OpenRGBHub::OpenRGBHub(const char * name, USBModule * usb_module, RGBManager * rgb_manager, FanManager * fan_manager, SensorManager * sensor_manager)
{
    this->name           = name;
    this->usb_module     = usb_module;
    this->rgb_manager    = rgb_manager;   
    this->fan_manager    = fan_manager;
    this->sensor_manager = sensor_manager;
}

void OpenRGBHub::begin()
{
    usb_module->begin(&read_packet, &write_packet);
    if (rgb_manager) rgb_manager->begin();
    if (fan_manager) fan_manager->begin();
    last_usb_update = getTick();
    last_rgb_update = getTick();
    last_fan_update = getTick();
}

void OpenRGBHub::loop()
{
    if (usb_module->readPacket(&read_packet))
    {
        switch (read_packet.command) 
        {
        case COMMAND_DEVICE_REPORT:
            sendDeviceReport(&write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_ZONE_REPORT:
            rgb_manager->parseGetZoneInfo(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_SET_LED:
            rgb_manager->parseSetLED(&read_packet);
            break;
        case COMMAND_FORCE_UPDATE:
            rgb_manager->updateZones();
            write_packet.size = OPENRGB_PROTOCOL_STANDARD_SIZE;
            write_packet.command = COMMAND_FORCE_UPDATE;
            usb_module->writePacket(&write_packet);
            last_rgb_update = getTick();
            break;
        case COMMAND_RESIZE_ZONE:
            rgb_manager->parseResize(&read_packet);
            break;
        case COMMAND_EFFECT_REPORT:
            rgb_manager->parseGetEffectInfo(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_SET_EFFECT:
            rgb_manager->parseSetEffect(&read_packet);
            break;
        case COMMAND_GET_LED:
            rgb_manager->parseGetLED(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_GET_EFFECT:
            rgb_manager->parseGetEffect(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_FAN_REPORT:
            fan_manager->parseGetFanInfo(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_SENSOR_REPORT:
            sensor_manager->parseGetSensorInfo(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_SET_FAN_STATE:
            fan_manager->parseSetState(&read_packet);
            break;
        case COMMAND_GET_FAN_STATE:
            fan_manager->parseGetState(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        case COMMAND_GET_FAN_FEEDBACK:
            fan_manager->parseGetSpeedFeedback(&read_packet, &write_packet);
            usb_module->writePacket(&write_packet);
            break;
        default:
            break;
        }
        last_usb_update = getTick();
    }

    /* Automatic update kicks in after $AUTOMATIC_UPDATE_DELAY ms since the last USB packet */ 
    if (((getTick() - last_usb_update) > AUTOMATIC_UPDATE_DELAY) and ((getTick() - last_rgb_update) > RGB_MANAGER_TARGET_PERIOD) and rgb_manager)
    {
        rgb_manager->updateEffects();
        rgb_manager->updateZones();
        last_rgb_update = getTick();
    }
    if (((getTick() - last_usb_update) > AUTOMATIC_UPDATE_DELAY) and ((getTick() - last_fan_update) > FAN_MANAGER_TARGET_PERIOD) and fan_manager)
    {
        fan_manager->update();
        last_fan_update = getTick();
    }
    
    return;
}

void OpenRGBHub::sendDeviceReport(hid_write_data_t * packet)
{
    packet->size = OPENRGB_PROTOCOL_STANDARD_SIZE;
    packet->command = COMMAND_DEVICE_REPORT;

    packet->data[0] = DEVICE_VERSION(0, 1); /* Firmware revision */

    if (rgb_manager) 
    {
        packet->data[1] = rgb_manager->getZoneNum();
        packet->data[2] = rgb_manager->getEffectNum();
    }
    else
    {
        packet->data[1] = 0; 
        packet->data[2] = 0;       
    }     
    
    if (fan_manager)
    {
        packet->data[3] = fan_manager->getFanNum();
    }
    else
    {
        packet->data[3] = 0;
    }

    if (sensor_manager)
    {
        packet->data[4] = sensor_manager->getSensorNum();
    }
    else
    {
        packet->data[4] = 0;
    }

    packet->data[5] = 0; /* Feature bitfield */

    uint8_t * name_pointer = (uint8_t*)name;

    for (int8_t i = 0; i < 56; i++)
    {
        packet->data[6 + i] = name_pointer[i];
        if (name_pointer[i] == '\0') break;
    }

    return;
}