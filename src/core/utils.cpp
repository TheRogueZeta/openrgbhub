#include "utils.hpp"

#ifdef ARDUINO

#include <Arduino.h>

uint32_t getTick()
{
    return millis();
}

#endif