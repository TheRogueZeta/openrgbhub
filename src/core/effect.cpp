#include "effect.hpp"

/* Copy parameter data */
Effect::Effect(effect_param_t * effect_param, Zone * zone_ptr)
{
    param = *effect_param;
    this->zone_ptr = zone_ptr;
    
    return;
}

effect_param_t * Effect::getParameters()
{
    return &param;
}

Effect::~Effect()
{
};