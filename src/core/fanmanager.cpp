#include "fanmanager.hpp"

FanManager::FanManager(Fan ** fan_list, uint8_t fan_list_size, SensorManager * sensors) {
    this->fan_list = fan_list;
    this->fan_list_size = fan_list_size;
    this->sensors = sensors;
}

void FanManager::begin()
{
    for (int i = 0; i < fan_list_size; i++)
    {
        fan_list[i]->begin();
    }
}

void FanManager::update()
{
    for (uint8_t i = 0; i < fan_list_size; i++)
    {
        fan_state_t * state = fan_list[i]->getState();
        if (state->function != FAN_FUNCTION_CURVE) continue;
        fan_list[i]->update(sensors->readSensor(state->sensor_index));
    }
}

uint8_t FanManager::getFanNum()
{
    return fan_list_size;
}

void FanManager::parseGetFanInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet) 
{
    uint8_t index = in_packet->read();
    fan_info_t * info = fan_list[index]->getInfo();

    out_packet->command = COMMAND_FAN_REPORT;
    out_packet->data[0] = index;
    out_packet->data[1] = info->features;

    for (uint8_t i = 0; i < 60; i++) {
        out_packet->data[2 + i] = info->name[i];
        if (info->name[i] == '\0')
        {
            return;
        }
    }

    return;
}

void FanManager::parseSetState(hid_read_data_t * in_packet)
{
    uint8_t fan_index = in_packet->read();
    fan_state_t state;
    state.function = in_packet->read(); 
    state.fan_mode = in_packet->read();

    if (state.function == FAN_FUNCTION_FIXED) 
    {
        state.fixed_speed  =  in_packet->read();
        state.fixed_speed |= (uint16_t)in_packet->read() << 8;
    }
    else
    {
        state.sensor_index = in_packet->read();
        state.curve_array_size = in_packet->read();
        for (int i = 0; i < state.curve_array_size; i++)
        {
            state.curve_array[i].sensor_val  = in_packet->read();
            state.curve_array[i].sensor_val |= (uint16_t)in_packet->read() << 8;
            state.curve_array[i].fan_val     = in_packet->read();
            state.curve_array[i].fan_val    |= (uint16_t)in_packet->read() << 8;
        }
    }

    fan_list[fan_index]->setState(state);

    return;
}

void FanManager::parseGetState(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t fan_index = in_packet->read();
    fan_state_t * state = fan_list[fan_index]->getState();
    out_packet->data[0] = fan_index;
    out_packet->data[1] = state->function;
    out_packet->data[2] = state->fan_mode;

    if (state->function == FAN_FUNCTION_FIXED) 
    {
        out_packet->data[3] = state->fixed_speed;
        out_packet->data[4] = state->fixed_speed >> 8;
    }
    else
    {
        out_packet->data[3] = state->sensor_index;
        out_packet->data[4] = state->curve_array_size;
        for (int i = 0; i < state->curve_array_size; i++)
        {
            out_packet->data[5 + i * 4] = state->curve_array[i].sensor_val;
            out_packet->data[6 + i * 4] = state->curve_array[i].sensor_val >> 8;
            out_packet->data[7 + i * 4] = state->curve_array[i].fan_val;
            out_packet->data[8 + i * 4] = state->curve_array[i].fan_val >> 8;
        }
    }

    return;
}

void FanManager::parseGetSpeedFeedback(hid_read_data_t * in_packet, hid_write_data_t * out_packet)
{
    uint8_t index = in_packet->read();
    uint16_t speed = fan_list[index]->getRPMFeedback();

    out_packet->command = COMMAND_GET_FAN_FEEDBACK;
    out_packet->data[0] = index;
    out_packet->data[1] = speed;
    out_packet->data[2] = speed >> 8;

    return;
}
