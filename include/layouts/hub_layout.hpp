/*

hub_layout.hpp

Defines for TheRogueZeta's board for openrgbhub

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#define DATA_PIN_CHANNEL_1 2
#define DATA_PIN_CHANNEL_2 4
#define DATA_PIN_CHANNEL_3 7
#define DATA_PIN_CHANNEL_4 8
#define DATA_PIN_CHANNEL_5 15
#define DATA_PIN_CHANNEL_6 3

#define TEMP_SENSOR_PIN_1 A0
#define TEMP_SENSOR_PIN_2 A1
#define TEMP_SENSOR_PIN_3 A2
#define TEMP_SENSOR_PIN_4 A3

#define FAN_RPM_PIN_1 14
#define FAN_RPM_PIN_2 16
#define PWM_FAN_PIN_1 6
#define PWM_FAN_PIN_2 10