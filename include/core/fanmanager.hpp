/*

fanmanager.hpp

The FanManager class

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "fan.hpp"
#include "sensormanager.hpp"
#include "usbmodule.hpp"
#include "openrgbprotocol.hpp"

class FanManager {
public:
    FanManager(Fan ** fan_list, uint8_t fan_list_size, SensorManager * sensors);

    void begin();
    void update();

    uint8_t getFanNum();

    /* Protocol functions */
    void parseGetFanInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet);
    void parseSetState(hid_read_data_t * in_packet);
    void parseGetState(hid_read_data_t * in_packet, hid_write_data_t * out_packet);
    void parseGetSpeedFeedback(hid_read_data_t * in_packet, hid_write_data_t * out_packet);

private:
    Fan ** fan_list;
    uint8_t fan_list_size;
    SensorManager * sensors;
};
