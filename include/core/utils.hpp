/*

utils.hpp

Cross-platform utilities.

Author: Pol Rius (alpemwarrior)

*/

#include <stdint.h>

#define ARRSIZE(a) (sizeof(a) / sizeof(*a))
uint32_t getTick();