/*

rgbmanager.hpp

Manager for all rgb zone objects. 

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "usbmodule.hpp"
#include "zone.hpp"

class RGBManager
{
public:
    RGBManager(Zone ** zone_list, 
               uint8_t zone_num, 
               effect_constructor_t ** effect_constructor_list, 
               uint8_t effect_num
    );

    void begin();

    /* Protocol functions */
    void parseSetLED(hid_read_data_t * packet);
    void parseGetLED(hid_read_data_t * in_packet, hid_write_data_t * out_packet);

    void parseSetEffect(hid_read_data_t * packet);
    void parseGetEffect(hid_read_data_t * in_packet, hid_write_data_t * out_packet);

    void parseResize(hid_read_data_t * packet);
    
    void parseGetZoneInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet);
    void parseGetEffectInfo(hid_read_data_t * in_packet, hid_write_data_t * out_packet);

    uint8_t getZoneNum();
    uint8_t getEffectNum();

    /* Update all zones */
    void updateZones();

    /* Update all effects */
    void updateEffects();
    
private:
    /* List of zone handles */
    Zone ** zone_list;
    uint8_t zone_num;
    
    /* List of effect constructor handles */
    effect_constructor_t ** effect_constructor_list;
    uint8_t effect_num;
};