/*

effect.hpp

Header for openrgbhub class

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "openrgbprotocol.hpp"

class Zone;

class Effect
{
public:
    Effect(effect_param_t * effect_param, Zone * zone_ptr);
    virtual ~Effect();
    
    /* Methods to implement */

    /* Effect loop */
    virtual void update() = 0;

    /* This event is triggered when the zone that it handles is resized */
    virtual void zoneHasBeenResized() = 0;

    effect_param_t * getParameters();

protected:
    effect_param_t param;
    Zone * zone_ptr;
};

/* Contains an info struct and factory function */
typedef struct 
{
    effect_info_t info;
    Effect* (*constructor)(effect_param_t*, Zone*);;
}effect_constructor_t;

/* Include so that effects don't have to */
#include "zone.hpp"