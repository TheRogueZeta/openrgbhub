/*

sensor.hpp

Fan interface

Author: Pol Rius (alpemwarrior)

*/

#pragma once

#include "openrgbprotocol.hpp"

class Sensor {
public:
    virtual void begin() = 0;
    virtual uint16_t read() = 0;
    
    sensor_info_t * getSensorInfo();

protected:
    sensor_info_t sensor_info;
};
