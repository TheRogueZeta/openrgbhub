# OpenRGBHub 

## What is it?

OpenRGBHub allows you create a flexible and powerful RGB controller compatible with OpenRGB. OpenRGBHub uses a USB connection for communication. 

## How does it compare to other open-source projects

OpenRGBHub is highly modular. Any microcontroller can be added as long as an implementation 
for USBModule and compatible Zone implementations are provided. 

OpenRGBHub works around the limitations of other projects by using a new protocol designed with flexibility in mind. 

## Compatible devices

For now, only a configuration for the arduino pro micro is provided, but other atmega32u4-based boards can be added easily. 

## Getting started
### Requirements

You will need to install [PlatformIO for VSCode](https://platformio.org/platformio-ide).

### Clone this repository

Open VSode, press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>P</kbd> and enter `gitcl`, then select "Git: Clone" and press Enter. Paste or type `https://gitlab.com/alpemwarrior/openrgbhub.git` into the repository URL field and select a folder. Don't make a new one, VSCode will automatically create it in the selected folder.

### Open PlatformIO project

Select the PlatformIO symbol on the toolbar on the left, then select "Projects & Configuration".

![Picture of the PlatformIO sidebar: Projects & Configuration](documentation/images/pio_projects.png)

Next, click "Add Existing".

![Picture of the PlatformIO home: "Add Existing" button is focused](documentation/images/pio_open_project.png)

Now, you have to select the "OpenRGBHub" folder **inside of the repository** (e.g. `openrgbhub/OpenRGBHub`) and click "Open 'OpenRGBHub'".

![Picture of the PlatformIO "Open Project" Dialog](documentation/images/pio_open_project_dialog.png)

If the project does not show up like below, close the tab by pressing <kbd>Ctrl</kbd> + <kbd>W</kbd> and open it again through the sidebar like before to refresh its contents. Click "Open".

![Picture of the PlatformIO Project "Open" button](documentation/images/pio_open_project_again_ugh.png)

### Configuration

By default, pins 2 and 4 are configured as "ARGB Zone 1" and "ARGB Zone 2" for up to 200 LEDs. If you're not using the OpenRGBHub PCB, you can hook up most ARGB components here by data pin. Neopixel, WS2812B, and all 5V ARGB computer parts (like fans etc.) should be compatible without changing configuration. You can change these settings in [OpenRGBHub/src/main/main.cpp](OpenRGBHub/src/main/main.cpp) and [OpenRGBHub/include/layouts/hub_layout.hpp](OpenRGBHub/include/layouts/hub_layout.hpp).

#### ARGB 5V header

| ▪  | ▪ |     | ▪ |
| -  | - | -   | - |
| 5V | D | N/A | G |

Hook up both 5V and ground (G) to your power supply. It's tempting to just use the power that's provided by the MCU, but you might risk your controller if you do this. You can get SATA power breakout boards for a few bucks, or you can just salvage an old SATA power connector from an old drive. You can optionally also connect the PSU ground with your controller's ground, which could potentially solve some flickering problems but is generally not needed. Other than that, only your data (D) pin should be connected to your controller. If you connect your ARGB fan's data pin to your controller's pin 2, it will be "ARGB Zone 1". If you connect it to pin 4, it will be "ARGB Zone 2".

### Compile and upload

Now, you should be able to compile and upload OpenRGBHub through the PlatformIO sidebar on the left under "Project Tasks". Expand the board you're working with, then upload. Don't upload "Default".

![](documentation/images/pio_upload.png)

## Todo

- Add examples
- Improve docs
- Provide binary files?
- License
- Better explanation
