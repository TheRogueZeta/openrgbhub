#include "openrgbhub.hpp"
#include "usbmodule_arduinohid.hpp"
#include "fastled_zone.hpp"
#include "test_effect.hpp"
#include "arduino_analog_zone.hpp"
#include "arduino_pwm_fan.hpp"
#include "arduino_temperature_sensor.hpp"
#include "hub_layout.hpp"
#include "utils.hpp"
#include <Arduino.h>

FastLEDZone<1, 100> zone_1("Digital zone 1");
FastLEDZone<2, 100> zone_2("Digital zone 2");

ArduinoAnalogZone zone_3(3, 5, 6, (unsigned char*)"Analog zone 1");

Zone * zone_array[] = 
{
  &zone_1,
  &zone_2,
  &zone_3
};

effect_constructor_t * effect_list[] = 
{
  &TestEffectConstructor
};

RGBManager rgb_manager(zone_array, 
                       ARRSIZE(zone_array),          
                       effect_list, 
                       ARRSIZE(effect_list)
                      );

ArduinoHIDUSBModule hid_module;

ArduinoPWMFan fan_1(LEONARDO_PIN_6, 7, "Arduino fan 1");

Fan * fan_array[]
{
  &fan_1
};

ArduinoTempSensor sensor_1(A0, 10000, 100000, 4242.0, "My temperature sensor");

Sensor * sensor_array[]
{
  &sensor_1
};

SensorManager sensor_manager = SensorManager(sensor_array, ARRSIZE(sensor_array));

FanManager fan_manager = FanManager(fan_array, ARRSIZE(fan_array), &sensor_manager);

OpenRGBHub hub = OpenRGBHub("My hub", &hid_module, &rgb_manager, &fan_manager, &sensor_manager);

/* Arduino sketches use setup() and loop() instead of main() */
void setup() {
  hub.begin();
  Serial.begin(9600);
}

void loop() {
  hub.loop();
}
